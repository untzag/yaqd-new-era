# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/).

## [Unreleased]

### Changed
- avro transition

## [2020.05.0]

### Added
- initial release

[Unreleased]: https://gitlab.com/yaq/yaqd-horiba/-/compare/v2020.05.0...master
[2020.05.0]: https://gitlab.com/yaq/yaqd-horiba/-/tags/v2020.05.0
